package hashfiles

import (
	"hash/fnv"
	"io"
	"os"
)

type HashToFiles map[uint64][]string

type MaybeHash struct {
	path string
	hash uint64
	err  error
}

// Hash the file at 'path'.
func hashFile(path string) (uint64, error) {
	file, err := os.Open(path)
	if err != nil {
		return 0, err
	}
	defer file.Close()
	buffer := make([]byte, 4096, 4096)
	hash := fnv.New64a()
	for {
		n, err := file.Read(buffer)
		if n > 0 {
			hash.Write(buffer[:n])
		}
		if err == io.EOF {
			break
		} else if err != nil {
			return 0, err
		}
	}

	return hash.Sum64(), nil
}

func hashFileAsync(path string, response chan MaybeHash) {
	hash, err := hashFile(path)
	if err != nil {
		response <- MaybeHash{err: err}
	} else {
		response <- MaybeHash{path: path, hash: hash, err: nil}
	}
}
